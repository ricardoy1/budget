﻿namespace budget.test
open System
open NUnit.Framework
open Budget
open Budget.Types
open Budget.Calculus

[<TestFixture>]
type CalculusTests() = 

    [<Test>]
    member x.``GetWordsFromText should return a 1 element array when input is empty``() =
        // Arrange
        let input = ""

        // Act
        let result = Calculus.getWordsFromText input

        // Assert
        Assert.AreEqual([|""|], result)

    [<Test>]
    member x.``GetWordsFromText should return a 1 element array when input is 1 word``() =
        // Arrange
        let input = "Hello"

        // Act
        let result = Calculus.getWordsFromText input

        // Assert
        Assert.AreEqual([|"Hello"|], result)

    [<Test>]
    member x.``GetWordsFromText should return a 2 element array when input is 2 words``() =
        // Arrange
        let input = "Hello world"

        // Act
        let result = Calculus.getWordsFromText input

        // Assert
        Assert.AreEqual([|"Hello"; "world"|], result)

    [<Test>]
    member x.``existsIntersection should be false for two empty lists``() =
        // Arrange
        // Act
        let result = Calculus.existsIntersection [||] [||]
 
        // Assert
        Assert.IsFalse(result)

    [<Test>]
    member x.``existsIntersection should be false when the first list is empty``() =
        // Arrange
        // Act
        let result = Calculus.existsIntersection [||] [|"A"; "B"|]
 
        // Assert
        Assert.IsFalse(result)    

    [<Test>]
    member x.``existsIntersection should be false when the second list is empty``() =
        // Arrange
        // Act
        let result = Calculus.existsIntersection [|"A"; "B"|] [||]
 
        // Assert
        Assert.IsFalse(result)  


    [<Test>]
    member x.ShouldGroupTotalPerBudget() =
        // Arrange
        // Act
        let totals = 
            Calculus.getTotalPerBudget [(1.0m, "a"); (1.0m, "a"); (1.0m, "b"); (5.0m, "a"); (10.0m, "b")]

        // Assert
        Assert.AreEqual ([("a", 7.0M); ("b", 11.0M)], totals)

    [<Test>]
    member x.ShouldGetShoppingForColes() =
        // Arrange
        let shoppingBudget = {Name = "shopping"; Limit = 1000m; Schedule = Never; Keywords = [|"Coles"|]}
        let budgetX = {Name = "X"; Limit = 30m; Schedule = Never; Keywords = [||]}
        let budgetY = {Name = "Y"; Limit = 90m; Schedule = Never; Keywords = [||]}
        let shoppingTransaction = {Description = "Coles"; Amount = 100m; Date = DateTime.Now}

        // Act
        let transactionAmountWithBudget = Calculus.getTransactionWithBudget [shoppingBudget; budgetX; budgetY] shoppingTransaction

        // Assert
        let expectedTransactionAmountBudget = (100m, Some(shoppingBudget))

        Assert.AreEqual(expectedTransactionAmountBudget, transactionAmountWithBudget)
          