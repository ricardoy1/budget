﻿namespace Budget
open Budget.Types
open System.IO

module DataProvider =

    open System
    open System.Collections.Generic

    let readRow (row:string) =
        let values = row.Replace('\"', ' ').Split([|','|])
        values

    let transformRowToTransaction transactionRow =
        let date = Array.get transactionRow 0 |>  System.DateTime.Parse
        let amount = Array.get transactionRow 1 |> System.Decimal.Parse
        let description = Array.get transactionRow 2
        let transaction = { Amount = amount; Date = date; Description = description }
        transaction

    let rec processTransactionLines lines =
        match lines with
            | [] -> []
            | "" :: _ -> []
            | currentLine :: remaining ->
                let transaction = currentLine |> readRow |> transformRowToTransaction
                let transactionsRest = processTransactionLines remaining
                transaction :: transactionsRest

    
    let readTransactions (filePath:string) =
        File.ReadAllLines(filePath)
            |> List.ofSeq
            |> processTransactionLines

    let transformRowToBudget budgetRow =
        let budgetName = Array.get budgetRow 0
        let limit = Array.get budgetRow 1 |> System.Decimal.Parse
        let frequency = Array.get budgetRow 2
        let startDate = Array.get budgetRow 3 |> System.DateTime.Parse
        let keyWords = Array.get budgetRow 4

        let schedule = match frequency with
                       | "Once" -> Schedule.Once(startDate)
                       | "Fortnightly" -> Schedule.Repeatedly(startDate, TimeSpan.FromDays 14.0)
                       | "Monthly" -> Schedule.Repeatedly(startDate, TimeSpan.FromDays 30.0)
                       | _ -> failwith "Invalid frequency "
        let budget = {Limit = limit; Schedule = schedule; Name = budgetName; Keywords = keyWords.Split([|';'|]) }
        budget
      
                    

    let rec processBudgetLines lines =
        match lines with
            | [] -> []
            | "" ::_ -> []
            | currentLine :: remaining ->
                let budget = currentLine |> readRow |> transformRowToBudget
                let budgetsRest = processBudgetLines remaining
                budget :: budgetsRest

    let readBudgets (filePath:string) =
        File.ReadAllLines(filePath)
            |> List.ofSeq
            |> processBudgetLines