﻿namespace budget.test
open System
open NUnit.Framework
open Budget
open Budget.Types
open Budget.Calculus

[<TestFixture>]
type DataProviderTests() = 

    [<Test>]
    member x.ShouldReadTheTransactionRowComponents() =
        // Arrange
        let textRow = "30/03/2016,\"-37.68\",VISA DEBIT PURCHASE COLES"
        // Act
        let transactionRow = DataProvider.readRow textRow
        // Assert
        Assert.AreEqual(3, transactionRow.Length)

    [<Test>]
    member x.``Should get transaction from row``() =
        // Arrange
        let transactionRow = [|"01/01/2015"; "12.24"; "SUPER DESCRIPTION"|]

        // Act
        let transaction = DataProvider.transformRowToTransaction transactionRow

        // Assert
        Assert.AreEqual(DateTime.Parse("01/01/2015"), transaction.Date)
        Assert.AreEqual(12.24m, transaction.Amount)
        Assert.AreEqual("SUPER DESCRIPTION", transaction.Description)

    [<Test>]
    member x.``Should get budget from row``() =
        // Arrange
        let budgetRow = [|"Groceries"; "100"; "Fortnightly"; "01/01/2016";"coles;woollworth;aldi"|]

        // Act
        let budget = DataProvider.transformRowToBudget budgetRow

        //Assert
        Assert.AreEqual("Groceries", budget.Name)
        Assert.AreEqual(100m, budget.Limit)
        Assert.AreEqual(Schedule.Repeatedly(DateTime.Parse("01/01/2016"), TimeSpan.FromDays 14.0), budget.Schedule)
        Assert.AreEqual([|"coles";"woollworth";"aldi"|], budget.Keywords)