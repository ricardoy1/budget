﻿namespace Budget

module Calculus =

    open Types
    open System
    open System.Collections.Generic

    let getWordsFromText (text:String) =
        text.Split([|' '|])

    let existsIntersection (words:String[]) (keywords:String[]) =
        Array.exists (fun x -> Array.exists (fun y -> y = x) words) keywords

    let containsAtLeastOneKeywordInText (text:String) (keywords:String[]) =
        let words = getWordsFromText text
        keywords |> existsIntersection words

    let getBudgetForTransaction transaction budgets =
        budgets |> Seq.tryFind (fun x -> containsAtLeastOneKeywordInText transaction.Description x.Keywords)

    let getTransactionWithBudget budgets transaction =
        let currentBudget = getBudgetForTransaction transaction budgets
        (transaction.Amount, currentBudget)

    let mapForPresentation pairs =
        pairs 
            |> List.map (fun x -> match x with 
                                    | (t, Some(x))->(t, x.Name)
                                    | (t, _) -> (t, ""))

    let getTotalPerBudget transactionBadgetPairs =
        transactionBadgetPairs 
            |> List.groupBy snd
            |> List.map (fun (key, (values:(decimal*string)list)) -> (key, List.sumBy fst values))

    let calculate budgets transactions =
        transactions 
            |> List.map (getTransactionWithBudget budgets)
            |> mapForPresentation
            |> getTotalPerBudget