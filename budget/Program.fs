﻿open System
open Budget
open Types

[<EntryPoint>]
let main argv = 
    let trxs = DataProvider.readTransactions "./../../../source/ANZ.csv"
    let budgets = DataProvider.readBudgets  "./../../../source/budgets.csv"

    let result = Calculus.calculate budgets trxs
    result |> List.map  (fun b -> printfn "%s %f" (fst b) (snd b)) |> ignore

    printfn "%A" argv
    0