﻿namespace Budget

module Types =

    open System
    open System.Collections.Generic

    type Schedule =
        | Never
        | Once of DateTime
        | Repeatedly of DateTime * TimeSpan

    type Budget =
        {
            Limit : decimal 
            Schedule : Schedule
            Name: string
            Keywords: string[]
        }

    type Transaction =
        {
            Amount: decimal
            Date: DateTime
            Description: string
        }