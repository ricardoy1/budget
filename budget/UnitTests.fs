﻿namespace Test
open System
open NUnit.Framework
open Budget

[<TestFixture>]
type Test() = 

    [<Test>]
    member x.TestCase() =
        let totals = 
            Calculus.getTotalPerBudget [(1.0m, "a"); (1.0m, "a"); (1.0m, "b"); (5.0m, "a"); (10.0m, "b")]
        Assert.AreEqual ([("a", 7.0M); ("b", 11.0M)], totals)