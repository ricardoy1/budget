# README #

This is a console application that allows you to control your budget by crossing information with your bank statements.

### What is this repository for? ###

This repository is an F# excercise that allows to implement a small practical solution and demonstrate how easy it can get to create a small app in F# as well as to resolve some specific problems.

### How do I get set up? ###

Define your budget.csv and download your bank statement in order to analize where you have over/under-spend money and therefore plan the next cycle better.

Budget.csv is based on keywords that the console app should find in the bank statement in order to associate that specific transaction to one specific budget.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Ricardo Youssef
ricardoy1@gmail.com
http://rsyoussef.com/